# GitLab Command Generator

🐯 This tool is in the early stages. It is currently available at [gitlab.carranza.engineer](https://gitlab.carranza.engineer) and [brie.gitlab.io/gitlab-command-generator](https://brie.gitlab.io/gitlab-command-generator/).

## ℹ️ About

Many thanks to the folks at [Sourcegraph](https://sourcegraph.com/search): this tool reuses much of the code from their [support-generator](https://github.com/sourcegraph/support-generator) tool. This tool was modified for use with GitLab by [Brie Carranza](https://brie.dev) who continues to maintain it. This is aimed at folks who administer self-managed GitLab instances of all deployment types. 

## ✅ Everyone can contribute.

  - ⭐️ Star this project. 
  - ✍️ Open an issue to ask a question or propose a change. 
  - 💾 Open a PR to fix or improve something.


