{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "github.schema.json#",
    "title": "List of Commands",
    "description": "A list of frequently-used commands for each GitLab deployment type.",
    "allowComments": true,
    "type": "object",
    "additionalProperties": false,
    "Omnibus": {
        "Function": {
            "value": "Function",
            "command": "[CHOOSE FUNCTION]",
            "description": "Default option"
        },
        "Check GitLab Status": {
            "command": "gitlab-ctl status",
            "description": "Check the status of GitLab on the current node.",
            "docsurl": "https://docs.gitlab.com/omnibus/maintenance/#get-service-status"
        },
        "Create backup of GitLab": {
            "command": "gitlab-backup create",
            "description": "Create a backup of the GitLab instance.",
            "docsurl": "https://docs.gitlab.com/ee/raketasks/backup_gitlab.html"
        },
        "Open GitLab Rails Console": {
            "command": "gitlab-rails console",
            "description": "Open a prompt at the GitLab Rails Console.",
            "docsurl": "https://docs.gitlab.com/ee/administration/operations/rails_console.html#starting-a-rails-console-session"
        },
        "Reconfigure GitLab": {
            "command": "gitlab-ctl reconfigure",
            "description": "Reconfigure GitLab to pick up changes in gitlab.rb.",
            "docsurl": "https://docs.gitlab.com/ee/administration/restart_gitlab.html#omnibus-gitlab-reconfigure"
        },
        "Connect to the bundled PostgreSQL console": {
            "command": "gitlab-rails dbconsole --database main",
            "description": "Open a prompt at the gitlabhq_production database",
            "docsurl": "https://docs.gitlab.com/omnibus/settings/database.html#connecting-to-the-bundled-postgresql-database"
        },
        "Display status of database migrations":{
            "command": "gitlab-rake db:migrate:status",
            "description": "Check on database migrations, useful post-upgrade",
            "docsurl": "https://docs.gitlab.com/ee/administration/raketasks/maintenance.html#display-status-of-database-migrations"
        },
        "Run incomplete database migrations": {
            "command": "gitlab-rake db:migrate",
            "description": "Run any stuck database migrations, useful post-upgrade",
            "docsurl": "https://docs.gitlab.com/ee/administration/raketasks/maintenance.html#run-incomplete-database-migrations"
        },
        "Check that database values can be decrypted": {
            "command": "gitlab-rake gitlab:doctor:secrets",
            "description":"Checks whether the contents of gitlab-secrets.json decrypt database values properly.",
            "docsurl": "https://docs.gitlab.com/ee/administration/raketasks/check.html#verify-database-values-can-be-decrypted-using-the-current-secrets"
        }
    },  
    "Docker": {
        "Function": {
            "value": "Function",
            "command": "[CHOOSE FUNCTION]",
            "description": "Default option"
        },
        "Get Logs": {
            "command": "docker logs",
            "option": "$CONTAINER",
            "description": "Get the logs of a container.",
            "docsurl": "https://docs.gitlab.com/ee/install/docker.html#diagnose-potential-problems"
        },
        "List Containers": {
            "command": "docker ps -A",
            "option": "",
            "description": "Get a list of containers."
        },
        "Run Bash Shell": {
            "command": "docker exec -it",
            "option": "$CONTAINER",
            "command2": "bash",
            "description": "This allows you to execute an interactive bash shell on a running container.",
            "docsurl": "https://docs.gitlab.com/ee/install/docker.html#diagnose-potential-problems"
        },
        "Access pgsql database": {
            "command": "docker container exec -it",
            "option": "$CONTAINER",
            "command2": "gitlab-psql",
            "description": "This allows you to access the main database and run psql commands on a running container.",
            "docsurl": "https://docs.gitlab.com/omnibus/settings/database.html#connecting-to-the-bundled-postgresql-database"
        },
        "Kill Container": {
            "command": "docker kill",
            "option": "$CONTAINER",
            "description": "Kill one or more running containers (SIGKILL)"
        },
        "Stop Container" : {
            "command": "docker stop",
            "option": "$CONTAINER",
            "description": "Stop one or more running containers (SIGTERM)"
            
        },
        "Start Container": {
            "command": "docker start",
            "option": "$CONTAINER",
            "description": "Start a container."
        },
        "Restart Container": {
            "command": "docker restart",
            "option": "$CONTAINER",
            "description": "Stop and start a container in one command."
        },
        "Run Interactive Shell": {
            "command": "docker exec -it",
            "option": "$CONTAINER",
            "command2": "shell",
            "description": "This allows you to execute an interactive shell on a running container."
        }
    },
    "Docker Compose": {
        "Function": {
            "value": "Function",
            "command": "[CHOOSE FUNCTION]",
            "description": "Default option"
        },
        "Get Logs": {
            "command": "docker compose logs",
            "option": "$CONTAINER",
            "description": "Get the logs of a container."
        },
        "List Containers": {
            "command": "docker compose ps -a",
            "option": "",
            "description": "Get a list of containers."
        },
        "Kill Containers": {
            "command": "docker compose kill",
            "option": "",
            "description": "Get a list of containers."
        },
        "Start GitLab": {
            "command": "docker compose up -d",
            "option": "",
            "description": "Builds, (re)creates, starts, and attaches to containers for GitLab. `-d` flag starts the container in the detached mode where the container runs in the background."
        },
        "Shutdown GitLab": {
            "command": "docker compose down",
            "option": "",
            "description": "Stops containers and removes containers, networks, volumes, and images created by docker-compose up -d."
        },
        "Run Bash Shell": {
            "command": "docker compose exec -it",
            "option": "$CONTAINER",
            "command2": "bash",
            "description": "This allows you to execute an interactive bash shell on a running container."
        }
    },
    "Kubernetes": {
        "Function": {
            "value": "Function",
            "command": "[CHOOSE FUNCTION]",
            "description": "Default option"
        },
        "Find Toolbox pods": {
            "command": "kubectl get pods -lapp=toolbox",
            "description": "Find Toolbox pods",
            "docsurl": "https://docs.gitlab.com/charts/troubleshooting/kubernetes_cheat_sheet.html#gitlab-specific-kubernetes-information"
        },
        "Open Rails console (from toolbox pod)": {
            "command": "/srv/gitlab/bin/rails console",
            "description": "Open GitLab Rails Console from within the toolbox pod",
            "docsurl": "https://docs.gitlab.com/charts/troubleshooting/kubernetes_cheat_sheet.html#gitlab-specific-kubernetes-information"
        },
        "Tail logs for all webservices pods": {
            "command": "kubectl logs -f -l app=webservice --all-containers=true --max-log-requests=50",
            "description": "All containers in the webservices pods (Rails, Workhorse)",
            "docsurl": "https://docs.gitlab.com/charts/troubleshooting/kubernetes_cheat_sheet.html#gitlab-specific-kubernetes-information"
        },
        "Get Logs": {
            "command": "kubectl logs",
            "option": "$POD",
            "description": "Get the logs of a specified pod."
        },
        "List Pods": {
            "command": "kubectl get pods -A",
            "option": "",
            "description": "Get a list of pods within the cluster and their health status."
        },
        "Describe Pod": {
            "command": "kubectl describe pod",
            "option": "$POD",
            "description": "Display detailed information about the status of a single pod"
        },
        "Get all cron jobs": {
            "command": "kubectl get cronjobs"
        },
        "Get Events": {
            "command": "kubectl get events",
            "option": "",
            "description": "List all events in the cluster’s history."
        },
        "Delete a Pod": {
            "command": "kubectl delete pod",
            "option": "$POD",
            "description": "Delete a failing pod so that it will get recreated, possibly on a different node."
        },
        "List Persistent Volume Claims (PVCs)": {
            "command": "kubectl get pvc",
            "option": "",
            "description": "List all Persistent Volume Claims (PVCs) and their statuses."
        },
        "List Persistent Volumes (PVs)": {
            "command": "kubectl get pv",
            "option": "",
            "description": "List all Persistent Volumes (PVs) that have been provisioned."
        },
        "Run Interactive Shell": {
            "command": "kubectl exec -it",
            "option": "$POD",
            "command2": " -- sh",
            "description": "This allows you to execute an interactive shell on a Pod."
        },
        "Get initial root password": {
            "command": "kubectl get secrets | grep initial-root",
            "description": "Find the name of the secret that contains the initial root password.",
            "docsurl": "https://docs.gitlab.com/charts/installation/secrets.html#initial-root-password"
        },
        "Show pod metrics": {
            "command": "kubectl top pod -- containers",
            "option": "$POD",
            "description": "This allows you to show metrics for a given pod and it's containers."
        },
        "Check Storage Class": {
            "command":"kubectl get storage class",
            "option": "",
            "description":"This allows you to check the existence of a storage class and it's name."
        },
        "List container images": {
            "command":"kubectl get pods -o jsonpath='{.items[*].spec.containers[*].image}'",
            "option": "",
            "description":"List the container images running in the cluster."
        }
    }
}
