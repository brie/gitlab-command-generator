# Local Development

> This is the information needed to develop this site locally. 

## Commands

In the directory where you have cloned this project:

| Command               | Action                                             |
| :-------------------- | :------------------------------------------------- |
| `npm install`         | Installs dependencies                              |
| `npm run start`       | Starts local dev server at `localhost:3000`        |
| `npm run build`       | Build your production site to `./dist/`            |


When you like how things look during `npm run start`, run `npm run build` to generate the `build` directory that would get deployed via GitLab Pages. 